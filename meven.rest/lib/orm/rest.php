<?php
namespace Meven\Rest\ORM;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class RestTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> name varchar(100)
 * <li> address string
 * <li> created_at datetime optional
 * <li> updated_at datetime optional
 * </ul>
 *
 * @package Meven\Rest\ORM
 **/

class RestTable extends Main\Entity\DataManager
{
	private static $instance = NULL;

	private function __construct()
	{
	}

	public static function getInstance()
    {
        if (self::$instance === NULL) {
            self::$instance = new RestTable();
        }

        return self::$instance;
    }

	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'meven_rest';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('REST_ENTITY_ID_FIELD'),
			),
			'name' => array(
				'data_type' => 'string',
                'required' => true,
				'title' => Loc::getMessage('REST_ENTITY_NAME_FIELD'),
			),
			'address' => array(
				'data_type' => 'string',
                'required' => true,
				'title' => Loc::getMessage('REST_ENTITY_ADDRESS_FIELD'),
			),
			'updated_at' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('REST_ENTITY_UPDATED_FIELD'),
			),
			'created_at' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('REST_ENTITY_CREATED_FIELD'),
			),
		);
	}

    /**
     * @param Main\Entity\Event $event
     * @return Main\Entity\EventResult|void
     * @throws Main\ObjectException
     *
     * run before update and add field updated_at
     */
    public static function onBeforeUpdate(Main\Entity\Event $event)
    {
        $result = new Main\Entity\EventResult;
        $data = $event->getParameter("fields");
        $result->modifyFields(['updated_at' => new \Bitrix\Main\Type\DateTime()]);

        return $result;
    }

    /**
     * @param Main\Entity\Event $event
     * @return Main\Entity\EventResult|void
     * @throws Main\ObjectException
     *
     * run before create and add fields created_at, updated_at
     */
    public static function onBeforeAdd(Main\Entity\Event $event)
    {
        $result = new Main\Entity\EventResult;
        $data = $event->getParameter("fields");
        $result->modifyFields([
            'updated_at' => new \Bitrix\Main\Type\DateTime(),
            'created_at' => new \Bitrix\Main\Type\DateTime()
        ]);
        $data = $event->getParameter("fields");

        return $result;
    }
}
