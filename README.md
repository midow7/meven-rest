# meven.rest

Установка предусмотрена из папки /bitrix/modules/

Необходимо установить модуль и создать файл, на котором будет размещен компонент

```bash
$APPLICATION->IncludeComponent(
    "meven:rest",
    "",
    Array(
    )
);
```

# данные для модуля
Предполагается, что данные присылаются через POST в виде одномерного массива
В method передается действие (add / update / remove / get)

Для add:
name - название
address - адрес

Для update:
name - название
address - адрес
id - id элемента

Для remove:
id - id элемента

Для get ничего не нужно